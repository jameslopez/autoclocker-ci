# frozen_string_literal: true

require 'singleton'
require 'yaml'

class Config
  include Singleton

  CONFIG_KEYS = %i[clocking_type user password slack_token slack_user slack_channel timezone location holidays_region holidays_observed api_url api_login_url].freeze

  attr_reader :default

  def initialize
    @default = Struct.new(*CONFIG_KEYS).new(*config)
  end

  private

  def config
    CONFIG_KEYS.map { |key| ENV[key.upcase.to_s] }
  end
end
