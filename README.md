# Autoclocker CI

<div align="center">
  <img width="215" src="https://i.imgur.com/LQxThih.gif" alt="autoclocker logo" />
  
  **Autoclocker CI** is an automatic attendance clock in/out system
</div>

## Features

* Clocks in/out at scheduled times, every day
* Slack notifications (pings on error)
* Excludes weekends and holidays
* Runs as a background job
* Keeps any passwords/tokens in memory (requested at startup)

## Installation

Fork the project

Schedule 2 CI pipelines that will clock you in/out

You will need the following environment variables set:

```bash
CLOCKING_TYPE # `in` or `out`
USER # Clocking app user
PASSWORD # Clocking app password
SLACK_TOKEN
SLACK_USER # Slack user to be notified on error
SLACK_CHANNEL
TIMEZONE # Europe/Madrid
LOCATION # Default: MAD
HOLIDAYS_REGION # `es` or any from https://github.com/holidays/definitions/blob/master/es.yaml#L16
HOLIDAYS_OBSERVED # `true` https://github.com/holidays/holidays#check-for-observed-holidays
API_URL
API_LOGIN_URL
```

## TODO / Nice to have

* Tests
* Connect to google calendar
* Custom timings to clock in or out several times
* Make it API agnostic

## License

MIT
